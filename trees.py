from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_curve, roc_auc_score
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def train_test_split(df, df_test=None):
  df_x = df.ix[:, df.columns != 'y_true']
  df_y = df['y_true']

  if df_test is None:
    train_size = int(0.9 * len(df))

    train_x, train_y = df_x[:train_size].as_matrix(), df_y[:train_size].as_matrix()
    test_x, test_y = df_x[train_size:].as_matrix(), df_y[train_size:].as_matrix()
  else:
    df_test_x = df_test.ix[:, df_test.columns != 'y_true']
    df_test_y = df_test['y_true']

    train_x, train_y = df_x.values, df_y.values
    test_x, test_y = df_test_x.ix[:, df.columns != 'y_true'], df_test['y_true']

  print(np.sum(test_y == 0))

  return train_x, train_y, test_x, test_y


def train(train_x, train_y):
  clf = RandomForestClassifier(max_depth=3,
                               random_state=123123)
  clf.fit(train_x, train_y)

  return clf


def write_test_pred_tsv(test_y_pred, test_y_names):
  with open('test_results.tsv', 'w') as f:
    for y_p, name in zip(test_y_pred, test_y_names):
      s = '%s\t%d\n' % (name, y_p)
      f.write(s)


def main():
  df = pd.read_csv('Alex/all_features_train.csv', sep='\t')
  df = df.sample(frac=1).reset_index(drop=True)
  # df_cont = pd.read_csv('Alex/all_features_contour.csv', sep='\t')
  # df_cont.drop('y_true', 1, inplace=True)

  # df = pd.concat([df, df_cont], axis=1, join='inner')

  # df_test = pd.read_csv('Alex/all_features_test_names.csv', sep='\t')

  train_x, train_y, test_x, test_y = train_test_split(df)

  print(train_x.shape, test_x.shape)

  clf = train(train_x, train_y)

  test_y_pred = clf.predict(test_x)

  # write_test_pred_tsv(test_y_pred, test_y)
  # return

  test_y_score = clf.predict_proba(test_x)[:, 1]

  acc = accuracy_score(test_y, test_y_pred)
  auc = roc_auc_score(test_y, test_y_score)
  print('acc: %.4f, auc: %.4f' % (acc, auc))

  fp, tp, thresholds = roc_curve(test_y, test_y_score)
  plt.figure(1)
  plt.xlabel('False positive rate')
  plt.ylabel('True positive rate')
  plt.plot([0, 1], [0, 1], 'k--')
  plt.plot(fp, tp)
  plt.show()


if __name__ == '__main__':
  main()