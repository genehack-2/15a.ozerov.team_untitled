import numpy as np
import random

def predict(images):
  return random.randint(0, 1)


def predict_all(all_x):
  random.seed(1337)

  all_y = []

  for x in all_x:
    y_pred = predict(x)
    all_y.append(y_pred)

  return all_y