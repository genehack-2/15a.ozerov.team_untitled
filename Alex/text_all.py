import retrieve_features as rf
import numpy as np
import pandas as pd
import sys
HOME_DIR = '/Users/agalicina/Term10/genehack/'  ##change for your machine
sys.path.append(HOME_DIR+'genehack2016')
from process import *


to_apply = [
    #(lambda x: rf.get_Hough_lines_lens(x['nucleus'], par_len=3, par_gap=0.5), '{}_max_int.png', 'max_int' ),
    (lambda x: rf.get_intensity_max( x['pic_nucleus']), '{}_max_int.png', 'max_int' ),
    #(lambda x: rf.get_intensity_mean(x['pic_nucleus']), '{}_mean_int.png', 'mean_int' ),
    (lambda x: rf.get_intensity_sum( x['pic_nucleus']), '{}_sum_int.png', 'sum_int' ),
    (lambda x: rf.get_intensity_max( x['pic_foci']), '{}_max_int_f.png', 'max_int_f' ),
    #(lambda x: rf.get_intensity_mean(x['pic_foci']), '{}_mean_int_f.png', 'mean_int_f' ),
    (lambda x: rf.get_intensity_sum( x['pic_foci']), '{}_sum_int_f.png', 'sum_int_f' )
]

to_apply.extend( [
    (lambda x, i=i, j=j: rf.get_Hough_lines_lens(x['nucleus'], par_len=i, par_gap=j),
             '{}'+'_hough_{0:.4f}_{1:.4f}.png'.format(i,j),
             'hough_{0:.4f}_{1:.4f}'.format(i,j))
    for i,j in [(0.0091, 0.0016)]
    #for i in np.arange(0.0031,0.01,0.001)
    #    for j in np.arange(0.0016,0.01,0.001)
    ])

mode = 'test'

to_apply.extend( [
    (lambda x: rf.calculate_all(x['nucleus_filename'], 1)[0],
             '{}'+'_truedef_{0:.2f}.png'.format(1),
             'truedef_{0:.2f}'.format(1,) ),
    (lambda x: rf.calculate_all(x['nucleus_filename'], 8)[1],
             '{}'+'_max_def.png',
             'max_def'),
    (lambda x: rf.calculate_all(x['nucleus_filename'], 8)[2],
             '{}'+'_sol.png',
             'sol')])#,
    #(lambda x: rf.calculate_all(x['nucleus_filename'], 8)[3],
    #         '{}'+'_jac.png',
    #         'jac'),

    #]
print(to_apply)
# rf.calculate_all(path, tresh)

#xss = []

features = {}
y_true = []

img_dir = '/Users/agalicina/Term10/genehack/test_set_sep/'
all_imgs, y_test= load_data(img_dir)
#img_dir = '/Users/agalicina/Term10/genehack/marked_1410_H2AX_Rad51_CM_05_1h_2h_4h_6h/'
#all_imgs1, y_test1= load_data(img_dir)
#img_dir = '/Users/agalicina/Term10/genehack/marked_1410_H2AZ_Rad51_CM_05_rest/'
#all_imgs2, y_test2 = load_data(img_dir)
#img_dir = '/Users/agalicina/Term10/genehack/batch_3/'
#all_imgs3, y_test3 = load_data(img_dir)
#
#all_imgs.extend(all_imgs1)
#all_imgs.extend(all_imgs2)
#all_imgs.extend(all_imgs3)
#y_test.extend(y_test1)
#y_test.extend(y_test2)
#y_test.extend(y_test3)


for j in range(len(to_apply)):
    i = to_apply[j]
    res = rf.my_apply_tolist(i[0], all_imgs=all_imgs, y_test=y_test)
    #print res[1]
    #print res[2]
    #rf.plot_hist(res[2], res[1], i[1].format('hist'), bins=50, hist_kws={"alpha":0.5} )
    #rf.plot_roc( res[2], res[1], i[1].format('roc'))
    features[i[2]] = np.array(res[1])

y_true = ['_'.join( all_imgs[j]['nucleus_filename'].split('/')[-1].split('_')[1:] ).split('.')[0] for j in range(len(all_imgs)) ]

features['y_true'] = y_true

features = pd.DataFrame(features)
features.to_csv('all_features_{}.csv'.format(mode), sep='\t', quoting=False, index=False)