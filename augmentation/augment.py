#!/usr/bin/python2

import sys, os, json
import numpy as np
import cv2
import math

#stackoverflow
def rotate_image(image, angle):
  '''Rotate image "angle" degrees.

  How it works:
    - Creates a blank image that fits any rotation of the image. To achieve
      this, set the height and width to be the image's diagonal.
    - Copy the original image to the center of this blank image
    - Rotate using warpAffine, using the newly created image's center
      (the enlarged blank image center)
    - Translate the four corners of the source image in the enlarged image
      using homogenous multiplication of the rotation matrix.
    - Crop the image according to these transformed corners
  '''

  diagonal = int(math.sqrt(pow(image.shape[0], 2) + pow(image.shape[1], 2)))
  offset_x = (diagonal - image.shape[0])/2
  offset_y = (diagonal - image.shape[1])/2
  dst_image = np.zeros((diagonal, diagonal, 3), dtype='uint8')
  image_center = (diagonal/2, diagonal/2)

  R = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  dst_image[offset_x:(offset_x + image.shape[0]), \
            offset_y:(offset_y + image.shape[1]), \
            :] = image
  dst_image = cv2.warpAffine(dst_image, R, (diagonal, diagonal), flags=cv2.INTER_CUBIC)

  # Calculate the rotated bounding rect
  x0 = offset_x
  x1 = offset_x + image.shape[0]
  x2 = offset_x
  x3 = offset_x + image.shape[0]

  y0 = offset_y
  y1 = offset_y
  y2 = offset_y + image.shape[1]
  y3 = offset_y + image.shape[1]

  corners = np.zeros((3,4))
  corners[0,0] = x0
  corners[0,1] = x1
  corners[0,2] = x2
  corners[0,3] = x3
  corners[1,0] = y0
  corners[1,1] = y1
  corners[1,2] = y2
  corners[1,3] = y3
  corners[2:] = 1

  c = np.dot(R, corners)

  x = int(c[0,0])
  y = int(c[1,0])
  left = x
  right = x
  up = y
  down = y

  for i in range(4):
    x = int(c[0,i])
    y = int(c[1,i])
    if (x < left): left = x
    if (x > right): right = x
    if (y < up): up = y
    if (y > down): down = y
  h = down - up
  w = right - left

  cropped = np.zeros((w, h, 3), dtype='uint8')
  cropped[:, :, :] = dst_image[left:(left+w), up:(up+h), :]
  return cropped

def make_grayscale(img):
  return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def make_scaled(img, scale):
  return cv2.resize(img, (scale, scale), interpolation = cv2.INTER_CUBIC)

def make_rotated(img, angle):
  return rotate_image(img, angle)

def make_intensity_transform(img, multiple):
  def transform(intval):
    return min(255, max(0, int(multiple * intval + 0.5))) 

  ret = img
  for i in range(0, ret.shape[0]):
    for j in range(0, ret.shape[1]):
      ret[i, j] = transform(ret[i, j])

  return ret

def make_sample(img, angle, multiple, scale):
  # scale first
  scaled_img = make_scaled(img, scale)
  rotated_img = make_rotated(scaled_img, angle)  
  grayscale_img = make_grayscale(make_scaled(rotated_img, scale))
  return make_intensity_transform(grayscale_img, multiple)
  # transformed_img = make_intensity_transform(grayscale_img, multiple)
  # return make_scaled(transformed_img, scale)

def make_feature(img_nucleus, img_foci, angle, multiple, scale):
  sample_nucleus = make_sample(img_nucleus, angle, multiple, scale)
  sample_foci    = make_sample(img_foci, angle, multiple, scale)
  feature = np.zeros(2 * scale * scale)
  for i in range(0, sample_nucleus.shape[0]):
    for j in range(0, sample_nucleus.shape[1]):
      feature[i * scale + j] = 1.0 * sample_nucleus[i, j] / 255.0
  for i in range(0, sample_foci.shape[0]):
    for j in range(0, sample_foci.shape[1]):
      feature[scale * scale + i * scale + j] = 1.0 * sample_foci[i, j] / 255.0
  return feature

def make_features(img_nucleus, img_foci, good):
  # SCALE = 24
  # ANGLES = 8
  # INTENSITIES = 4
  # INTENSITY_FROM = 0.8
  # INTENSITY_TO   = 1.2 

  SCALE = 24
  ANGLES = 1
  INTENSITIES = 1
  INTENSITY_FROM = 1.0
  INTENSITY_TO   = 1.0  

  features = []
  answers = []
  for i_angle in range(ANGLES):
    angle = 360.0 * i_angle / ANGLES
    for i_intensity in range(INTENSITIES):
      intensity = INTENSITY_FROM + (INTENSITY_TO - INTENSITY_FROM) * i_angle
      # print 'making sample...', i_angle, i_intensity
      features.append(make_feature(img_nucleus, img_foci, angle, intensity, SCALE))
      if (good):
        answers.append(np.array([0.0, 1.0])) # [0,1] - good
      else:
        answers.append(np.array([1.0, 0.0])) # [1,0] - bad
  return (np.vstack(features), np.vstack(answers))

def process_batch(batch_file, dump_prefix):
  batch = {}
  with open(batch_file) as js:
    batch = json.load(js)
  
  batch_answers = []
  batch_features = []

  print 'Images: %d good, %d bad' % (len(batch['good']), len(batch['bad']))

  total_images = len(batch['good']) + len(batch['bad'])
  processed_images = 0  

  for ans in ['good', 'bad']:
    for item in batch[ans].keys():
      print 'Processing', item, '(%s)' % ans
      # print batch['good'][item]
      nucleus_img_path = os.path.join(os.path.split(batch_file)[0], batch[ans][item]['nucleus'])
      foci_img_path = os.path.join(os.path.split(batch_file)[0], batch[ans][item]['foci'])
      
      nucleus_img = cv2.imread(nucleus_img_path)
      foci_img = cv2.imread(foci_img_path)
      features, answers = make_features(nucleus_img, foci_img, ans == 'good')
      batch_features.append(features)
      batch_answers.append(answers)

      processed_images += 1
      print '[%d/%d] (%.2lf %%)' % (processed_images, total_images, 100.0 * processed_images / total_images)

  batch_answers = np.vstack(batch_answers)
  batch_features = np.vstack(batch_features)

  print batch_answers.shape
  print batch_features.shape
  
  features_dump = os.path.join(
      os.path.split(batch_file)[0], 
      '%s_%s_features.pkl' % (os.path.split(batch_file)[1], dump_prefix))
  answers_dump = os.path.join(
      os.path.split(batch_file)[0], 
      '%s_%s_answers.pkl' % (os.path.split(batch_file)[1], dump_prefix))

  batch_answers.dump(answers_dump)
  batch_features.dump(features_dump)

# image_path = '/home/kbulatov/genehack2016/batch_3/bad_pics/pic_nucleus_20141016_183831__005_001.png'

if __name__ == '__main__':
  batch_path = sys.argv[1]
  prefix = sys.argv[2]
  print 'Batch path: ', batch_path, 'prefix: ', prefix
  process_batch(batch_path, prefix)
