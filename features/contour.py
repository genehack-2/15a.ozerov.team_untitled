import os
from math import sqrt
import cv2
import numpy as np
from matplotlib import pyplot as plt

def find_contour(img):
    ret, thresh = cv2.threshold(img,127,255,0)
    image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    return contours[0]

def get_area(cnt):
    area = cv2.contourArea(cnt)
    return area

def get_perimeter(cnt):
    perimeter = cv2.arcLength(cnt,True)
    return perimeter

def plot_contour(img, cnt, path):
    img1 = img.copy()
    cv2.drawContours(img1,[cnt],0,(0,0,255),2)
    cv2.imwrite(path, img1)

    '''cv2.namedWindow("imc")
    cv2.imshow("imc", img1)
    cv2.waitKey(0)'''

def get_defects(cnt):
    hull = cv2.convexHull(cnt,returnPoints = False)
    defects = cv2.convexityDefects(cnt,hull)
    return defects
    
def plot_and_count_defects(defects, cnt, path):
    img1 = img.copy()
    true_def_count = 0
    true_tresh = 10

    n=0
    for i in range(defects.shape[0]):
        n+=1
        s,e,f,d = defects[i,0]
        start = tuple(cnt[s][0])
        end = tuple(cnt[e][0])
        far = tuple(cnt[f][0])
        med = (min(start[0],end[0])+(abs(start[0]-end[0])/2), min(start[1],end[1])+(abs(start[1]-end[1])/2))
        depth = sqrt(((med[0]-far[0])**2)+((med[1]-far[1])**2))
        if n != 1:
            if depth > true_tresh:
                true_def_count += 1
                cv2.circle(img1,far,2,[0,0,255],-1) #red!
            else:
                cv2.circle(img1,far,2,[255,0,0],-1) #blue!
            cv2.circle(img1,med,2,[255,0,255],-1)
            cv2.line(img1,start,end,[0,255,0],1)

    '''cv2.imshow('img',img1)
    cv2.waitKey(0)
    cv2.destroyAllWindows()'''

    cv2.imwrite(path, img1)
    return true_def_count

folder = '../processed_images/bad_pics/'
files = [x for x in os.listdir(folder) if x.startswith('nucleus')]
for i in files:
    img = cv2.imread(folder+i)
    imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    cnt = find_contour(imgray)
    print i
    if not cv2.isContourConvex(cnt):
        defects = get_defects(cnt)
        print plot_and_count_defects(defects, cnt, '../processed_images/opencv/'+i)
    
