import os
from os.path import join, abspath

from classification import predict_all

from sklearn import metrics
from skimage.io import imread, imsave

from argparse import ArgumentParser
import shutil

def load_image(data_subdir, key_filename, t):
  f = join(data_subdir, '%s_%s' % (t, key_filename))
  img = imread(f)
  return img, f


def load_single_cell(data_subdir, key_filename):
  cell = {'key_filename': key_filename}

  for t in ['nucleus', 'pic_nucleus', 'pic_foci']:
    cell[t], f = load_image(data_subdir, key_filename, t)
    cell[t + '_filename'] = f

  return cell


def load_subdir_data(data_subdir, y):
  print('loading subdir data from %s' % data_subdir)
  keys = []
  for f in os.listdir(data_subdir):
    prefix = 'nucleus_'
    if f.startswith(prefix):
      key = f[len(prefix):]
      keys.append(key)

  print('total %d keys' % len(keys))

  cur_x, cur_y = [], []

  for k in keys:
    cell = load_single_cell(data_subdir, k)

    cur_x.append(cell)
    cur_y.append(y)

  return cur_x, cur_y


def load_data(data_dir):
  print('loading data from %s' % data_dir)

  subdir_types = [('bad_pics', 0), ('good_pics', 1)]

  all_x, all_y = [], []
  for subdir, y in [(join(data_dir, d), y) for d, y in subdir_types]:
    cur_x, cur_y = load_subdir_data(subdir, y)

    all_x.extend(cur_x)
    all_y.extend(cur_y)

  return all_x, all_y


def process_all(all_x, all_y, output_dir):
  print('predicting...')

  all_y_pred = predict_all(all_x)

  fp, fn, thresholds = metrics.roc_curve(all_y, all_y_pred, pos_label=1)
  auc = metrics.roc_auc_score(all_y, all_y_pred)

  print('FP = %.4f, FN = %.4f, AUC = %.4f' % (fp[1], fn[1], auc))

  if os.path.exists(output_dir):
    shutil.rmtree(output_dir)
  os.mkdir(output_dir)
  os.mkdir(join(output_dir, 'fp'))
  os.mkdir(join(output_dir, 'fn'))

  for x, y, y_pred in zip(all_x, all_y, all_y_pred):
    if y == y_pred:
      continue

    error_type = 'fp' if y == 0 else 'fn'
    if output_dir is not None:
      save_dir = join(output_dir, error_type)
      save_path = join(save_dir, x['key_filename'])
      imsave(save_path, x['pic_nucleus'])


def main():
  parser = ArgumentParser()
  parser.add_argument('--markup-dir', type=str, default='processed_images')
  parser.add_argument('--output-dir', type=str, default='output')
  args = parser.parse_args()

  all_x, all_y = load_data(args.markup_dir)

  process_all(all_x, all_y, args.output_dir)


if __name__ == '__main__':
  main()
