#!/usr/bin/python2

import os, sys, json
import numpy as np
import cv2

from sklearn import svm
from sklearn.ensemble import RandomForestClassifier

# >>> from sklearn import svm
# >>> X = [[0, 0], [1, 1]]
# >>> y = [0, 1]
# >>> clf = svm.SVC()
# >>> clf.fit(X, y)  
def load_augmented_train_data(pkl_basenames):
  all_x, all_y = [], []
  for pkl in pkl_basenames:
    all_x.append(np.load(pkl + '_features.pkl'))
    all_y.append(np.load(pkl + '_answers.pkl'))
  return np.vstack(all_x).astype(np.float32), np.vstack(all_y).astype(np.int32)

if __name__ == '__main__':
  train_batches = [
    "../db/batch_3.json_test1"
  ]
  eval_batches = [
    "../db/processed_images.json_test1"
  ]
  train_x, train_y = load_augmented_train_data(train_batches)
  eval_x, eval_y = load_augmented_train_data(eval_batches)

  train_ans = [0] * train_y.shape[0]
  for i in range(len(train_ans)):
    if train_y[i][0] > 0:
      train_ans[i] = 0 # bad
    else:
      train_ans[i] = 1

  eval_ans = [0] * eval_y.shape[0]
  for i in range(len(eval_ans)):
    if eval_y[i][0] > 0:
      eval_ans[i] = 0 # bad
    else:
      eval_ans[i] = 1

  c = RandomForestClassifier(n_estimators=10, criterion='gini', max_depth=7)
  c.fit(train_x, train_ans)
  # c = svm.SVC(kernel='rbf')
  # c.fit(train_x, train_ans)

  #evaluation

  predictions = [0] * eval_y.shape[0]
  negatives = 0
  positives = 0
  fn = 0
  fp = 0
  accuracy = 0
  for i in range(len(eval_ans)):
    # print list(eval_x[i])
    prediction = c.predict([list(eval_x[i])])[0]
    if eval_ans[i]: # if good
      positives += 1
    else: # if bad
      negatives += 1
    if prediction == eval_ans[i]:
      accuracy += 1
    else:
      if eval_ans[i]: # if good
        fp += 1
      else: # if bad
        fn += 1

  print 'FP: %lf' % (1.0 * fp / positives)
  print 'FN: %lf' % (1.0 * fn / negatives)
  print 'AC: %lf' % (1.0 * accuracy / (positives + negatives))

    # predictions[i] = c.predict([list(x[i])])

  # print [x[0] for x in list(predictions)]
  
