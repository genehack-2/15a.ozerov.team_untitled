#!/usr/bin/python2

import os, sys, json

BATCHES = [
  'batch_3',
  'good_bad__results',
  'marked_1410_H2AX_Rad51_CM_05_1h_2h_4h_6h',
  'marked_1410_H2AZ_Rad51_CM_05_rest',
  'processed_images'
]

def describe_dir(dirpath):
  files = os.listdir(dirpath)
  cells = set([x[8:] for x in files if x.startswith('nucleus')])
  desc = { }
  for cell in cells:
    desc[cell] = {
      'nucleus': os.path.join(dirpath, 'pic_nucleus_' + cell),
      'foci':    os.path.join(dirpath, 'pic_foci_' + cell)
    }
  return desc

def process_batch(batch):
  bad_dir = os.path.join(batch, 'bad_pics')
  good_dir = os.path.join(batch, 'good_pics')
  bad_desc = describe_dir(bad_dir)
  good_desc = describe_dir(good_dir)
  desc = {
    'bad': bad_desc,
    'good': good_desc
  }
  with open(batch + '.json', 'w') as js:
    js.write(json.dumps(desc, indent = 2))

if __name__ == '__main__':
  for batch in BATCHES:
    process_batch(batch)
